import { StyleSheet } from "react-native";
import Colors from "../../assets/styles/colors";

export const styles = StyleSheet.create({

    boxContainer: {
        height: 294,
        width: 324,
        borderWidth: 2,
        padding: 30,
        borderColor: Colors.secondary.lightGray4,
        alignItems: "flex-start",
        flexDirection: "column",
        position: "relative"
    },
    title: {
        fontSize: 26,
        color: Colors.primary.black,
        marginBottom: 15,
    },
    text: {
        fontSize: 18,
        color: Colors.secondary.darkGray1,
    }

})


export const BtnImage = StyleSheet.create({
    common: {
        width: 20,
        height: 20,
        marginRight: 10,
        backgroundColor: "transparent",
        resizeMode: "contain"
    }
});

export const BtnContainer = StyleSheet.create({
    common: {
        width: 260,
        height: 60,
        justifyContent: "center",
        alignContent: "center",
        flexDirection: "row",
        borderWidth: 2,
        position: "absolute",
        bottom: 30,
        left: 30
    },
    primary: {
        backgroundColor: Colors.primary.green,
    },
    default: {
        borderColor: Colors.secondary.white,
        backgroundColor: "transparent"
    },
    secondary: {
        backgroundColor: Colors.secondary.darkGray1
    },
    tertiaty: {
        borderColor: Colors.secondary.darkGray4,
        backgroundColor: Colors.secondary.white
    },
});

export const BtnText = StyleSheet.create({
    common: {
        fontFamily: "Arial",
        fontWeight: "bold",
        fontSize: 16,
    },
    primary: {
        color: Colors.secondary.white
    },
    secondary: {
        color: Colors.secondary.white
    },
    tertiaty: {
        color: Colors.secondary.darkGray4
    },
    default: {
        color: Colors.secondary.white
    },
});
