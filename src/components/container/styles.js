import { StyleSheet } from "react-native";

export default StyleSheet.create({
    common: {

    },
    header: {
        height: 140,
        backgroundColor: "black",
    },
    footer: {
        height: 150,
    },
    footerBar: {
        height: 50,
        paddingVertical:15,
        paddingRight: 25,
        borderTopWidth:1,
        borderColor:'#ffffff',
        flexDirection: 'row',
        backgroundColor: '#CCCCCC', 
       justifyContent: 'space-between', 
    },
    headerBG: {
        resizeMode: "cover",
        paddingTop: 30,
        paddingHorizontal: 25,
        paddingBottom: 10,
        flex: 1,
    },
    flexRow: {
        flexDirection: "row"
    },
    flexColumn: {
        flexDirection: "column"
    },
    card: {
        flexDirection: 'row',
        justifyContent:'space-around'
    },
    horizontalCenteredContainer: {
        flex: 1,
    },
    centeredContainer: {
        flex: 1
    },
    startScreenLinksContainerTop: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-end",
    },
    startScreenLinksContainerBottom: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center"
    },
    productsDocuments: {
        height: 50,
        borderTopWidth:1,
        borderColor: "#CCCCCC",
        flexDirection:'column-reverse',

    }

});