
/**
 * Auto generated component for selfcare_mac
 * Author: jodelismarko
 * Date: 1/5/2020
 */
    
import React, { useState, useEffect } from "react";
import { View, Text, Button } from "react-native";
import styles from "./styles";
    
export default ({ navigation, props }) => {
    // Component state values
    const [counter, setCounter] = useState(0);

    /**
     * Add code to run when the component is mounted.
     * If need to run this code when some state is updated, add those states to the array given as argument.
     * Ex: ...}, [counter]);
     */
    useEffect(() => {
        // Code to run when mounted or updated
    
        // Return a function to run before unmount the component
        // This must be usefull to remove event listeners or clear some states
        return () => {
            // Code to run before unmount the component
        };
    }, []);
        
    
    return (
        <View style={styles.container}>
            <Text style={styles.title}>table</Text>
            <Text>You clicked {counter} times.</Text>
            <Button title="Click me to increment" onPress={() => setCounter(counter + 1)} />
        </View>
    );
};
