
/**
 * Auto generated component for selfcare_mac
 * Author: jodelismarko
 * Date: 1/5/2020
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        fontSize: 30
    }
});
