import Image from "../cachedImage";
import React, {useState} from "react";
import { View, Text } from "react-native";
import { styles, BtnImage, BoxContainer } from "./styles";
import { buildComponents } from "../../mappings/componentsBuilder";

export default ({ navigation,parentProps, styleName, props: { components, title, text, image }}) => {
    const [visible, setVisible] = useState(null);

    function showElements(){
        setVisible(!visible)
    }

    return (
        <View style={{...BoxContainer.common, ...BoxContainer[styleName] }}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                { visible ? <Text style={styles.titleActive}>{title}</Text> : <Text style={styles.title}>{title}</Text>}
                
                {image != null &&
                    <View onTouchEnd={showElements}>
                        { visible ? <Image props={{ url: image }} style={{ ...BtnImage.common}} /> : <Image props={{ url: image }} style={{ ...BtnImage.common}} />}  
                    </View>
                }
            </View>
            <Text style={styles.text}>{text}</Text>
            {visible && 
                <View>
                    {buildComponents(navigation, components, parentProps)}
                </View>
            }
        </View>
    );
};
