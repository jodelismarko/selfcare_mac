import { StyleSheet } from "react-native";
import Colors from "../../assets/styles/colors";


export const BtnImage = StyleSheet.create({
    common: {
        width: 40,
        height: 40,
        top:0,
        right:0,
        position:'absolute',

    }
});

export const BoxContainer = StyleSheet.create({
    common: {
        paddingTop:20,
        paddingBottom:10,
        marginHorizontal:80,
        position: "relative",
        
    },
    withLine: {
        borderBottomWidth:2,
        borderColor: Colors.secondary.lightGray1
    }
});

export const styles = StyleSheet.create({
    title: {
        fontSize: 26,
        marginBottom: 15,
        color: Colors.primary.black

    },
    titleActive: {
        fontSize: 26,
        marginBottom: 15,
        color: Colors.primary.green

    },
    text: {
        fontSize: 18,
        marginRight:50,
        color: Colors.secondary.darkGray1
    }
})
