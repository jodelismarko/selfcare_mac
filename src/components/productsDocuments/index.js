import React from "react";
import { View, FlatList, Text } from "react-native";
import styles from "./styles";
import { buildComponent } from "../../mappings/componentsBuilder";

//IMPORTANT: Items should be an array of LinkButton or similar
export default ({ navigation, props: { components }, styleName }) => {

    return (
        <FlatList
            horizontal={true}
            style={styles[styleName]}
            data={components}
            renderItem={({ item }) => buildComponent(navigation, item, null)}
            ItemSeparatorComponent={() => {
                return (
                    <View style={{ height: 60, width: 1, backgroundColor: "#CCCCCC"}} />
                );
            }}
            keyExtractor={(item, index) => index.toString()}
        />
    );
};