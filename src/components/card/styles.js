import { StyleSheet } from "react-native";
import Colors from "../../assets/styles/colors";


export const styles = StyleSheet.create({

    boxContainer: {
        height: 680,
        width: 340,
        padding: 5,
        marginRight:20,
        alignItems: "flex-start",
        flexDirection: "column",
        position: "relative"
        
    },
    title: {
        fontSize: 26,
        color: Colors.primary.black,
        marginBottom: 15,
    },
    text: {
        fontSize: 18,
        color: Colors.secondary.darkGray1,
    }
})

export const BtnImage = StyleSheet.create({
    common: {
        width: 320,
        height: 350,
        marginRight: 10,
        backgroundColor: "transparent",
        resizeMode: "contain"
    }
});

export const BtnContainer = StyleSheet.create({
    common: {
        width: 320,
        height: 60,
        justifyContent: "center",
        alignContent: "center",
        flexDirection: "row",
        borderWidth: 2,
        borderColor: Colors.secondary.darkGray1,
        position: "absolute",
        bottom: 30,
        left: 5
    }
});

export const BtnText = StyleSheet.create({
    common: {
        fontFamily: "Arial",
        fontWeight: "bold",
        fontSize: 16,
    }
});
