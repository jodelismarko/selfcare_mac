import { StyleSheet } from "react-native";

export default StyleSheet.create({
    common: {
        flex: 1
    },
    footerNavigationBar: {
        flex: 1,
        paddingVertical:5,
        backgroundColor: '#EDEDED',
        flexDirection: "column",
    },
});
