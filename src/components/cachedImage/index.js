import React, { useState } from "react";
import { Image } from "react-native";
import Loader from "../suspenseLoader";
import styles from "./style";
// import MediaStorage from "../../../storage/mediaStorage";

export default ({ props: { url }, styleName, style }) => {
    const [source, setSource] = useState(true);

    // async function getImage() {
    //     const image = await MediaStorage.getFile(url);
    //     setSource(image);
    // }

    // useEffect(() => {
    //     getImage();
    // }, []);

    return (
        !source
            ? <Loader />
            : <Image source={{ uri: url }} style={{...style, ...styles.common, ...styles[styleName]}} />
    );
};
