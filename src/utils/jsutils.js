export default {
    sleep: (time) => new Promise(resolve => setTimeout(resolve, time)),

    getStringHashCode: (obj) => {
        if (typeof obj !== "string") {
            obj = JSON.stringify(obj);
        }

        let hash = 0;
        let char;

        for (let i = 0; i < obj.length; i++) {
            char = obj.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash |= 0;
        }

        return hash;
    }
};