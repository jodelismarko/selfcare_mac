export default {
    primary: {
        black: "#000000",
        green: "#81BC00",
        darkGreen: "#657F05"
    },
    secondary: {
        darkGray4: "#555555",
        darkGray3: "#333333",
        darkGray2: "#6F6F6F",
        darkGray1: "#999999",
        lightGray4: "#E5E5E5",
        lightGray3: "#CCCCCC",
        lightGray2: "#D6D6D6",
        lightGray1: "#EDEDED",
        white: "#FFFFFF"
    },
    alerts: {
        blue: "#2D73A8",
        red: "#FF3A3A",
        pink: "#FF8080"
    }
};