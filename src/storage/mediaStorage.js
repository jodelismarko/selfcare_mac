import { Config } from "../../app.config";
import axios from "axios";
import Utils from "../utils/jsutils";
// import AsyncStorage from "@react-native-community/async-storage";

//TODO: JL - media file storage
//https://www.npmjs.com/package/react-native-fs
class MediaStorage {
    constructor(basePath) {
        this.basePath = basePath;
    }

    async getFile(url) {
        const key = Utils.getStringHashCode(url);

        try {
            let b64File = null;// = await AsyncStorage.getItem(key);

            if (!b64File) {
                const { data } = await axios.get(url, {
                    responseType: "arraybuffer",
                    timeout: 30000
                });

                b64File = new Buffer(data, "binary").toString("base64");
            }

            return b64File;
        } catch (error) {
            console.error(error);
            return null;
        }
    }

    saveFile(name, data) {
        
    }

    removeFile(name) {

    }

    clear() {

    }
}

export default new MediaStorage(Config.mediaPath);
