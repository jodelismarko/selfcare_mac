const _sitecoreResponse = [
    {
        "name": "Home",
        "screenType": "StaticScreen",
        "style": "startScreen",
        "components": [{
            "name": "Container",
            "contents": {
                "style": "horizontalCenteredContainer",
                "items": [{
                    "name": "Image",
                    "path": "/app-body/sxa-staticscreen/app-main/row-1-1",
                    "contents": {
                        "image": {
                            "fieldType": "image",
                            "link": "https://oney.pt/-/media/Project/Oney-Sites/Oney-Home/logo.png"
                        },
                        "style": "startScreenLogo"
                    }
                }]
            }
        }, {
            "name": "Container",
            "style": "centeredContainer",
            "path": "/app-body/sxa-staticscreen/app-main/row-2-1",
            "contents": {
                "items": [{
                    "name": "Container",
                    "style": "startScreenLinksContainerTop",
                    "contents": {
                        "items": [{
                            "Link": {
                                "value": {
                                    "href": "/Area-Cliente",
                                    "text": "Entrar na Área Cliente",
                                    "anchor": "",
                                    "linktype": "internal",
                                    "class": "",
                                    "title": "",
                                    "target": "",
                                    "querystring": "",
                                    "id": "{35E50AF2-64A6-4F52-A74A-030EB67CECAF}"
                                }
                            },
                            "Image": {
                                "value": {
                                    "src": "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    "alt": ""
                                }
                            },
                            "id": "ea85b87c-5939-4f35-b234-e619da3f2235",
                            "itemname": "Entrar na Area Cliente",
                            "name": "LinkButton",
                            "type": "Button",
                            "style": "default"
                        }, {
                            "Link": {
                                "value": {
                                    "href": "/Alterar-PIN-Cartao",
                                    "text": "Alterar PIN Cartão",
                                    "anchor": "",
                                    "linktype": "internal",
                                    "class": "",
                                    "title": "",
                                    "target": "",
                                    "querystring": "",
                                    "id": "{075A4FE9-DCDB-48F2-B8D8-448741151162}"
                                }
                            },
                            "Image": {
                                "value": {
                                    "src": "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    "alt": "Alterar código Oney Contacto"
                                }
                            },
                            "id": "546a776b-de54-43a8-bebb-d63c64905634",
                            "itemname": "Alterar Pin Cartao",
                            "name": "LinkButton",
                            "style": "default",
                            "type": "Button"
                        }]
                    }
                }, {
                    "name": "Container",
                    "style": "startScreenLinksContainerBottom",
                    "contents": {
                        "items": [{
                            "Link": {
                                "value": {
                                    "href": "/Adesao",
                                    "text": "Adesão Cartão Oney Auchan",
                                    "anchor": "",
                                    "linktype": "internal",
                                    "class": "",
                                    "title": "",
                                    "target": "",
                                    "querystring": "",
                                    "id": "{8C3938DB-1A9B-4B78-99E5-144230C019AF}"
                                }
                            },
                            "Image": {
                                "value": {
                                    "src": "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    "alt": ""
                                }
                            },
                            "id": "b1d0aca3-6405-4a33-a090-e803cb8a0179",
                            "itemname": "Adesao Cartao Oney Auchan",
                            "name": "LinkButton",
                            "type": "Button",
                            "style": "primary"
                        }, {
                            "Link": {
                                "value": {
                                    "href": "/Descarregar-a-App",
                                    "text": "Descarregar a App",
                                    "anchor": "",
                                    "linktype": "internal",
                                    "class": "",
                                    "title": "",
                                    "target": "",
                                    "querystring": "",
                                    "id": "{E2084134-0654-4016-AB05-6EADFE1A5012}"
                                }
                            },
                            "Image": {
                                "value": {
                                    "src": "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    "alt": ""
                                }
                            },
                            "id": "5bf7019a-e96c-435c-8f41-2a9d23a83018",
                            "itemname": "Descarregar a App",
                            "name": "LinkButton",
                            "type": "Button",
                            "style": "default"
                        }]
                    }
                }]
            }
        }]
    }, {
        "name": "Area-Cliente",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": [
            {
                "name": "Container",
                "style":"card",
                "contents": {
                    "style": "horizontalCenteredContainer",
                    "items": [{
                        Link: {
                            value: {
                                "href": "www.todoscontam.pt",
                                "text": "Saber Mais",
                                "linktype": "external"
                            }
                        },
                        Image: {
                            value: {
                                src: "https://fakeimg.pl/320x350/ff0000/000",
                            }
                        },
                        title: "Gestão do Orçamento Familiar",
                        text: "Para informações sobre a gestão do orçamento familiar, consulte o portal Todos Contam. O site administrado pelo Banco de Portugal promove a utilização do Crédito Responsável e disponibiliza ferramentas online para a gestão orçamental",
                        buttonText: "btn cenas",
                        name: "Card",
                        style: "common"
                    },{
                        Link: {
                            value: {
                                "href": "https://www.clientebancario.bportugal.pt",
                                "text": "Saber Mais",
                                "linktype": "external"
                            }
                        },
                        Image: {
                            value: {
                                src: "https://fakeimg.pl/320x350/ff0000/000",
                            }
                        },
                        title: "Decisões Financeiras Informadas",
                        text: "Tem dúvidas sobre algum produto ou serviço bancário? Encontre as respostas que procura no portal Cliente Bancário. Integrado no site oficial do Banco de Portugal, disponibiliza informações pertinentes, para o consumidor, sobre o sistema bancário.",
                        buttonText: "btn cenas",
                        name: "Card",
                        style: "common"
                    },{
                        Link: {
                            value: {
                                "href": "https://www.asfac.pt/",
                                "text": "Saber Mais",
                                "linktype": "external"
                            }
                        },
                        Image: {
                            value: {
                                src: "https://fakeimg.pl/320x350/ff0000/000",
                            }
                        },
                        title: "Financiamento ao Consumo",
                        text: "A Associação das Instituições de Crédito Especializado promove a divulgação de informação sobre financiamento especializado ao consumo. Subscreva a newsletter Ter em conta e receba informação sobre dicas de finanças pessoais, entre muitas outras.",
                        buttonText: "btn cenas",
                        name: "Card",
                        style: "common"
                    }]
                }
            },{
            "name": "Container",
            "style":"productsDocuments",
            "contents": {
                "style": "horizontalCenteredContainer",
                "items": [{
                    name: "ProductsDocuments",
                    style: "productsDocumentsBar",
                    contents: {
                        items: [

                            {
                                Link: {
                                    value: {
                                        "href": "/Home",
                                        "text": "Condições  Gerais  do   Crédito.",
                                        "linktype": "external"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://files.fm/down.php?i=tfgk5gd4",
                                        style: "documents"
                                    },
                                    style: "documents"
                                },
                                name: "LinkButton",
                                style: "documents"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Home",
                                        "text": "Ficha de Informação Normalizada Europeia",
                                        "linktype": "external"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://files.fm/down.php?i=tfgk5gd4",
                                    }
                                },
                                name: "LinkButton",
                                style: "documents"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Home",
                                        "text": "Condições Gerais de Seguro e Proteção",
                                        "linktype": "external"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://files.fm/down.php?i=tfgk5gd4",
                                    }
                                },
                                name: "LinkButton",
                                style: "documents"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Home",
                                        "text": "Deveres de Informação sobre o Mediador de Seguros",
                                        "linktype": "external"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://files.fm/down.php?i=tfgk5gd4",
                                    }
                                },
                                name: "LinkButton",
                                style: "documents"
                            }
                        ]
                    }
                }]
            }
        }]
    }, {
        "name": "Alterar-PIN-Cartao",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": [{
            "name": "Container",
            "contents": {
                "style": "horizontalCenteredContainer",
                "items": [{
                    Link: {
                        value: {
                            "href": "/Home",
                            "text": "Home",
                            "linktype": "internal"
                        }
                    },
                    Image: {
                        value: {
                            src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                        }
                    },
                    title: "Cartão do Cidadão",
                    text: "Auto-preencha o formulário com os seus dados, através do leitor do Cartão do Cidadão.",
                    buttonText: "btn cenas",
                    name: "LinkBox",
                    style: "primary"
                }]
            }
        }]
    }, {
        "name": "Adesao",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    }, {
        "name": "Descarregar-a-App",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    },{
        "name": "Credito-Responsavel",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    }, {
        "name": "Informação-Resolução-Alternativa-Litigios",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    }, {
        "name": "Apoio-Cliente",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": [{
            "name":"Accordion",
            "style":"withLine",
            "title":"Email",
            "text":"Tem alguma dúvida sobre os nossos produtos e/ou serviços? Respondemos a todas as perguntas e estamos sempre disponíveis para os nossos Clientes: apoiocliente@oney.pt",
            "Image": {
                value: {
                    src: "https://files.fm/down.php?i=qghb3z79",
                }
            },
            "contents": {
                "items": [{
                    name: "FooterNavigation",
                    style: "footerNavigationBar",
                    contents: {
                        items: [{
                            Link: {
                                value: {
                                    "href": "/Home",
                                    "text": "Direitos relativos a Pagamento na Europa",
                                    "linktype": "external"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Home",
                                    "text": "Livros de reclamações Eletrônico",
                                    "linktype": "external"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Informação-Resolução-Alternativa-Litigios",
                                    "text": "Informação sobre Resolução Alternativa de Litigios",
                                    "linktype": "internal"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Apoio-Cliente",
                                    "text": "Apoio ao Cliente",
                                    "linktype": "internal"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }]
                    }
                },{
                    name: "Container",
                    style: "footerBar",
                    contents: {
                        items: [
                            {
                                Link: {
                                    value: {
                                        "href": "/Política-Privacidade-Proteção-Dados",
                                        "text": "© 2003-2016 Oney. Todos os direitos reservedos.",
                                        "linktype": "external"
                                    }
                                },
                                name: "LinkButton",
                                style: "common"
                            },
                            {
                                Link: {
                                    value: {
                                        "href": "/Política-Privacidade-Proteção-Dados",
                                        "text": "Política de Privacidade e Proteção de Dados",
                                        "linktype": "internal"
                                    }
                                },
                                name: "LinkButton",
                                style: "common"
                            }
                            
                        ]
                    }
                }
                
            ]}
        },{
            "name":"Accordion",
            "style":"withLine",
            "title": "Crédito",
            "text": "Gostamos muito de falar consigo! E através dos nossos telefones, estamos sempre preparados para dar um aconselhamento personalizado. Ligue-nos para a linha de crédito que pretende e tenha todas as respostas que procura.",
            "contents": {
                "items": []
            }
        },{
            "name":"Accordion",
            "style":"withLine",
            "title":"3x 4x Oney",
            "text":"Para mais esclarecimentos relacionados com a solução 3x 4x Oney, ligue para: 808 913 434 (dias úteis das 9h às 23h) ou através do email suportecliente@oney.pt.",
            "Image": {
                value: {
                    src: "https://files.fm/down.php?i=qghb3z79",
                }
            },
            "contents": {
                "items": [{
                    name: "FooterNavigation",
                    style: "footerNavigationBar",
                    contents: {
                        items: [{
                            Link: {
                                value: {
                                    "href": "/Home",
                                    "text": "Direitos relativos a Pagamento na Europa",
                                    "linktype": "external"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Home",
                                    "text": "Livros de reclamações Eletrônico",
                                    "linktype": "external"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Informação-Resolução-Alternativa-Litigios",
                                    "text": "Informação sobre Resolução Alternativa de Litigios",
                                    "linktype": "internal"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }, {
                            Link: {
                                value: {
                                    "href": "/Apoio-Cliente",
                                    "text": "Apoio ao Cliente",
                                    "linktype": "internal"
                                }
                            },
                            name: "LinkButton",
                            style: "footerNavigationLink"
                        }]
                    }
                }
            ]}
        },{
            "name":"Accordion",
            "style":"withLine",
            "title":"Seguros",
            "text":"Para questões relacionadas com os seus Seguros, use os contactos indicados em baixo.",
            "Image": {
                value: {
                    src: "https://files.fm/down.php?i=qghb3z79",
                }
            },
            "contents": {
                "items": [{
                    name: "Container",
                    style: "footerBar",
                    contents: {
                        items: [
                            {
                                Link: {
                                    value: {
                                        "href": "/Política-Privacidade-Proteção-Dados",
                                        "text": "© 2003-2016 Oney. Todos os direitos reservedos.",
                                        "linktype": "external"
                                    }
                                },
                                name: "LinkButton",
                                style: "common"
                            },
                            {
                                Link: {
                                    value: {
                                        "href": "/Política-Privacidade-Proteção-Dados",
                                        "text": "Política de Privacidade e Proteção de Dados",
                                        "linktype": "internal"
                                    }
                                },
                                name: "LinkButton",
                                style: "common"
                            }
                            
                        ]
                    }
                }
                
            ]}
        },{
            "name":"Accordion",
            "style":"withLine",
            "title": "Procuro uma loja. Onde devo ir?",
            "text": "Tem alguma dúvida sobre os nossos produtos e/ou serviços? Respondemos a todas as perguntas e estamos sempre disponíveis para os nossos Clientes: apoiocliente@oney.pt",
            "contents": {
            "items": []
            }
        },{
            "name":"Accordion",
            "style":"withLine",
            "title": "Geral",
            "text": "Av. José Gomes Ferreira, nº 9 | 1495-139 Algés",
            "Image": {
                value: {
                    src: "https://files.fm/down.php?i=qghb3z79",
                }
            },
            "contents": {
                "items": [{
                    Link: {
                        value: {
                            "href": "www.todoscontam.pt",
                            "text": "Saber Mais",
                            "linktype": "external"
                        }
                    },
                    Image: {
                        value: {
                            src: "https://fakeimg.pl/320x350/ff0000/000",
                        }
                    },
                    title: "Gestão do Orçamento Familiar",
                    text: "Para informações sobre a gestão do orçamento familiar, consulte o portal Todos Contam. O site administrado pelo Banco de Portugal promove a utilização do Crédito Responsável e disponibiliza ferramentas online para a gestão orçamental",
                    buttonText: "btn cenas",
                    name: "Card",
                    style: "common"
                }]
            }
        },{
            "name":"Accordion",
            "style":"common",
            "title": "Queremos dar-lhes voz",
            "text": "A sua opinião é importante. Se tem dúvidas ou sugestões que gostaria de nos colocar, escreva-nos para satisfacaocliente@oney.pt",
            "contents": {
            "items": []
            }
        }]
    }, {
        "name": "Política-Privacidade-Proteção-Dados",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    }, {
        "name": "Table",
        "screenType": "ScrollScreen",
        "headerType": "Header",
        "footerType": "Footer",
        "components": []
    }
];

const _sitecoreHeaders = [{
    name: "Header",
    type: "Container",
    style: "header",
    imageBackground: "https://preprodsite.oney.pt/-/media/Images/AreaCliente/BannerTopoAreaCliente/1560x300px_Topo-Area-Privada_v2.ashx",
    components: [{
        name: "Container",
        style: "flexRow",
        contents: {
            items: [
                {
                    name: "Image",
                    contents: {
                        image: {
                            fieldType: "image",
                            link: "https://oney.pt/-/media/Project/Oney-Sites/Oney-Home/logo.png"
                        },
                        style: "headerLogo"
                    }
                }, {
                    name: "NavigationBar",
                    style: "headerNavigationBar",
                    contents: {
                        items: [
                            {
                                Link: {
                                    value: {
                                        "href": "/Home",
                                        "text": "Home",
                                        "linktype": "internal"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    }
                                },
                                name: "LinkButton",
                                style: "navigationLink"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Adesao",
                                        "text": "Adesão Cartão Oney Auchan",
                                        "linktype": "internal"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    }
                                },
                                name: "LinkButton",
                                style: "navigationLink"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Descarregar-a-App",
                                        "text": "App",
                                        "linktype": "internal"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    }
                                },
                                name: "LinkButton",
                                style: "navigationLink"
                            }, {
                                Link: {
                                    value: {
                                        "href": "/Alterar-PIN-Cartao",
                                        "text": "Alterar PIN Cartão",
                                        "linktype": "internal"
                                    }
                                },
                                Image: {
                                    value: {
                                        src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                                    }
                                },
                                name: "LinkButton",
                                style: "navigationLink"
                            }
                        ]
                    }
                }, {
                    Link: {
                        value: {
                            href: "/Area-Cliente",
                            text: "Área Cliente",
                            linktype: "internal"
                        }
                    },
                    Image: {
                        value: {
                            src: "https://i1.wp.com/atorre.pt/wp-content/uploads/2019/10/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558pigg7yyye3.png?fit=280%2C279&ssl=1",
                        }
                    },
                    name: "LinkButton",
                    style: "navigationLink"
                }
            ]
        }
    }, {
        name: "Container",
        style: "flexRow",
        contents: {
            items: [{
                name: "Image",
                contents: {
                    image: {
                        fieldType: "image",
                        link: "https://oney.pt/-/media/Project/Oney-Sites/Oney-Home/logo.png_"
                    },
                    style: "headerBottomImage"
                }
            }]
        }
    }]
}];

const _sitecoreFooters = [{
    name: "Footer",
    type: "Container",
    style: "footer",
    components: [{
        name: "FooterNavigation",
        style: "footerNavigationBar",
        contents: {
            items: [{
                Link: {
                    value: {
                        "href": "/Precario",
                        "text": "Preçário",
                        "linktype": "external"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Codigo-Conduta",
                        "text": "Código de Conduta",
                        "linktype": "external"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Informacao-Mediacao",
                        "text": "Informação de Mediaçāo",
                        "linktype": "external"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Credito-Responsavel",
                        "text": "Crédito Responsável",
                        "linktype": "internal"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }]
        }
    },{
        name: "FooterNavigation",
        style: "footerNavigationBar",
        contents: {
            items: [{
                Link: {
                    value: {
                        "href": "/Home",
                        "text": "Direitos relativos a Pagamento na Europa",
                        "linktype": "external"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Home",
                        "text": "Livros de reclamações Eletrônico",
                        "linktype": "external"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Informação-Resolução-Alternativa-Litigios",
                        "text": "Informação sobre Resolução Alternativa de Litigios",
                        "linktype": "internal"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }, {
                Link: {
                    value: {
                        "href": "/Apoio-Cliente",
                        "text": "Apoio ao Cliente",
                        "linktype": "internal"
                    }
                },
                name: "LinkButton",
                style: "footerNavigationLink"
            }]
        }
    },{
        name: "Container",
        style: "footerBar",
        contents: {
            items: [
                {
                    Link: {
                        value: {
                            "href": "/Política-Privacidade-Proteção-Dados",
                            "text": "© 2003-2016 Oney. Todos os direitos reservedos.",
                            "linktype": "external"
                        }
                    },
                    name: "LinkButton",
                    style: "common"
                },
                {
                    Link: {
                        value: {
                            "href": "/Política-Privacidade-Proteção-Dados",
                            "text": "Política de Privacidade e Proteção de Dados",
                            "linktype": "internal"
                        }
                    },
                    name: "LinkButton",
                    style: "common"
                }
                
            ]
        }
    },{
        Link: {
            value: {
                "href": "/Home",
                "linktype": "external"
            }
        },
        Image: {
            value: {
                src: "https://files.fm/down.php?i=uy9uvzs8",
            }
        },
        name: "LinkButton",
        style: "toTop"
    }]
}];

export function _getDummyScreensData() {
    return {
        initialScreen: "/Table",
        screens: _sitecoreResponse.map(page => {
            return {
                name: "/" + page.name,
                type: page.screenType,
                header: page.headerType,
                footer: page.footerType,
                styleName: page.style,
                props: { components: page.components.map(c => parseComponentContents(c)) }
            }
        }),
        headers: _sitecoreHeaders.map(header => {
            return {
                name: header.name,
                type: header.type,
                styleName: header.style,
                props: {
                    components: header.components.map(c => parseComponentContents(c)),
                    bgImage: header.imageBackground
                }
            }
        }),
        footers: _sitecoreFooters.map(footer => {
            return {
                name: footer.name,
                type: footer.type,
                styleName: footer.style,
                props: { components: footer.components.map(c => parseComponentContents(c)) }
            }
        })
    };
}

function parseComponentContents(component) {
    const result = {
        type: component.name,
        styleName: component.style
    };

    switch (component.name) {
        case "Image":
            result.styleName = component.contents.style;
            result.props = { url: component.contents.image.link };
            break;
        case "Container":
            result.props = {
                components: component.contents.items.map(c => parseComponentContents(c)),
                bgImage: component.imageBackground
            };
            break;
        case "LinkButton":
            const link = component.Link.value;
            const image = component.Image && component.Image.value && component.Image.value.src;
            result.props = { title: link.text, href: link.href, linkType: link.linktype, image };
            break;
        case "NavigationBar":
            result.props = { components: component.contents.items.map(c => parseComponentContents(c)) };
            break;
        case "ProductsDocuments":
            result.props = { components: component.contents.items.map(c => parseComponentContents(c)) };
            break;
        case "FooterNavigation":
            result.props = { components: component.contents.items.map(c => parseComponentContents(c)) };
            break;
        case "LinkBox":
            const link2 = component.Link.value;
            const image2 = component.Image && component.Image.value && component.Image.value.src;
            result.props = { title: component.title, href: link2.href, linkType: link2.linktype, buttonText: link2.text, text: component.text, image: image2 };
            break;
        case "Card":
            const linkCard = component.Link.value;
            const imageCard = component.Image && component.Image.value && component.Image.value.src;
            result.props = { title: component.title, href: linkCard.href, linkType: linkCard.linktype, buttonText: linkCard.text, text: component.text, image: imageCard};
            break;
        case "Accordion":
            const imageAccordion2= component.Image && component.Image.value && component.Image.value.src;
            result.styleName = component.style;
            result.props = {
                components: component.contents.items.map(c => parseComponentContents(c)),
                image: imageAccordion2,
                title: component.title,
                text: component.text,
            };
            break;
        default:
            

    }

    return result;
}

/* Result example
{
    "initialScreen": "Home",
    "screens": [{
        "name": "Home",
        "type": "StaticScreen",
        "contents": [{
            "type": "Image",
            "props": { "url": "https://oney.pt/-/media/Project/Oney-Sites/Oney-Home/logo.png" }
        }, {
            "type": "Container",
            "props": {
                "items": [{
                    "type": "LinkButton",
                    "props": {
                        "title": "Entrar na Área Cliente",
                        "href": "/Area-Cliente",
                        "linkType": "internal",
                        "image": "http://auchan.local/-/media/Images/Icones/info.svg"
                    }
                }, {
                    "type": "LinkButton",
                    "props": {
                        "title": "Alterar PIN Cartão",
                        "href": "/Alterar-PIN-Cartao",
                        "linkType": "internal",
                        "image": "http://auchan.local/-/media/Images/Mensagens-Feedback/alterar_codigo_oney_contato.svg"
                    }
                }, {
                    "type": "LinkButton",
                    "props": {
                        "title": "Adesão Cartão Oney Auchan",
                        "href": "/Adesao",
                        "linkType": "internal",
                        "image": "http://auchan.local/-/media/Images/Mensagens-Feedback/confirmacao_dados_pessoais.svg"
                    }
                }, {
                    "type": "LinkButton",
                    "props": {
                        "title": "Descarregar a App",
                        "href": "/Descarregar-a-App",
                        "linkType": "internal",
                        "image": "http://auchan.local/-/media/Images/Mensagens-Feedback/alterar_telemovel.svg"
                    }
                }]
            }
        }]
    }, {
        "name": "Alterar-PIN-Cartao",
        "type": "ScrollScreen",
        "header": "TestHeader",
        "contents": [{
            "type": "Image",
            "props": { "url": "https://oney.pt/-/media/Project/Oney-Sites/Oney-Home/logo.png" }
        }]
    }]
}
*/