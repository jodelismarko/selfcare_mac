import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import LoadingScreen from "./src/screens/loadingScreen";
import StackNavigator from "./src/navigation/stackNavigation";
import Utils from "./src/utils/jsutils";

//TODO: Remove - DUMMY DATA
import { _getDummyScreensData } from "./temp/_dummyScreensData";

export default App = () => {

  const [errorLoading, setErrorLoading] = useState(false);
  const [appData, setAppData] = useState(null);

  const _dummyGetData = async () => {
    await Utils.sleep(500);

    // Uncomment the following line to force error
    // throw "Error";

    return _getDummyScreensData();
  }

  const loadData = async () => {
    setErrorLoading(false);
    setAppData(null);

    try {
      const data = await _dummyGetData();
      setAppData(data);
    } catch (err) {
      setErrorLoading(true);
    }
  };

  useEffect(() => {
    loadData();

    return () => {
      setErrorLoading(false);
      setAppData(null);
    };
  }, []);

  return (
    <>
      <SafeAreaView style={styles.safeArea}>
        {
          !appData
            ? <LoadingScreen error={errorLoading} retryAction={loadData} />
            : <StackNavigator config={appData} />
        }
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1
  },
  green: {
    color: "green"
  }
});

